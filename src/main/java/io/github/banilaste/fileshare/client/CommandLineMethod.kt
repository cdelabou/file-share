package io.github.banilaste.fileshare.client

import io.github.banilaste.duck.generated.FileManager
import io.github.banilaste.fileshare.common.InputPattern

class CommandExecutionException(message: String?) : Exception(message)

/**
 * Class containing a command line method to be called, check the provided arguments to
 * be valid and provide a descriptive string of the command
 */
data class CommandLineMethod(
    val name: String,
    val description: String,
    private val arguments: List<InputPattern>,
    private val handler: (args: List<String>, fileManager: FileManager) -> String?
) {
    init {
        var optionalFound = false
        arguments.forEach {
            if (it.optional) {
                optionalFound = true
            } else if (optionalFound) {
                throw Exception("optional arguments must be the last ones")
            }
        }
    }

    fun execute(parts: List<String>, fileManager: FileManager): String? {
        if (parts.size - 1 < arguments.count { !it.optional } || parts.size - 1 > arguments.size) {
            throw CommandExecutionException("argument size do not match")
        }

        // Check arguments
        parts.forEachIndexed { index, part ->
            if (index > 0 && !part.matches(arguments[index - 1].pattern)) {
                throw CommandExecutionException("invalid argument for ${arguments[index - 1].name}")
            }
        }

        // Call method
        return handler(parts, fileManager)
    }

    override fun toString(): String {
        val argumentList = arguments
            .joinToString(" ") { if (it.optional) "(${it.name})" else "[${it.name}]" }

        return "\t- $name $argumentList : $description"
    }


}