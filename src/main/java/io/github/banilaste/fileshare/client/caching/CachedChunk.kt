package io.github.banilaste.fileshare.client.caching

import java.nio.ByteBuffer
import kotlin.math.max
import kotlin.math.min

data class CachedChunk(
    var offset: Long, // offset
    var data: ByteArray // data content for this cache
) {
    // Aliases for data
    val size get() = data.size
    val rightOffset get() = data.size + offset

    /**
     * Following chunk if any
     */
    var next: CachedChunk? = null

    /**
     * Try to read the given amount of data from the cache
     */
    fun read(readOffset: Long, readSize: Int): ByteArray? {
        return when {
            readOffset < offset -> null

            // Not included in current chunk
            readOffset > rightOffset -> {
                next?.read(readOffset, readSize)
            }

            // Included in current chunk but overflows
            readOffset + readSize > rightOffset -> {
                null // Otherwise, gap in the cache
            }

            // Included in current chunk and no overflowing
            else -> {
                ByteBuffer
                    .allocate(readSize)
                    .put(data, (readOffset - offset).toInt(), readSize)
                    .array()

            }
        }
    }

    /**
     * Merge the current chunk with the given chunk, returns the first chunk of the resulting cache content.
     * The current (this) chunk should not have any following item
     *
     * Provided chunk is supposed to be well formed or null : following chunks have offset greater than its rightOffset
     */
    fun mergeInto(chunk: CachedChunk?): CachedChunk {
        // Chunk is null, nothing to perform
        if (chunk == null) {
            return this
        }

        if (this.next != null) {
            throw Exception("the current chunk cannot be merged if it already has a following chunk, please" +
                    " call merge on a new CachedChunk")
        }

        // If chunks overlap, prune the oldest one and merge both (the provided chunk by definition @see CacheEntry)
        if (this.offset <= chunk.rightOffset && this.rightOffset >= chunk.offset) {
            val start = min(offset, chunk.offset)
            val end = max(rightOffset, chunk.rightOffset)
            val newData = ByteBuffer.allocate((end - start).toInt())

            // Write data from chunk
            newData.position((chunk.offset - start).toInt())
            newData.put(chunk.data)

            // Overwrite data with current
            newData.position((offset - start).toInt())
            newData.put(data)

            // Put the result in the current chunk (current data is conserved
            this.data = newData.array()
            this.offset = start

            // If the current chunk goes futher than the provided one,
            // also merge with the next one
            return if (chunk.rightOffset < end) {
                this.mergeInto(chunk.next)
            } else {
                // Otherwise just take it's next value
                this.next = chunk.next
                this
            }
        }

        // If the provided chunk is the last one
        return if (chunk.offset < this.offset) {
            // We can keep chunk as first one, but the following wil be the result of a merge
            chunk.next = mergeInto(chunk.next)
            chunk
        } else {
            // Otherwise chunk can become the second item unchanged
            this.next = chunk
            this
        }
    }

    /**
     * Locate the next chunk involved with the given index
     */
    fun locate(offset: Long): CachedChunk? {
        var chunk: CachedChunk? = this

        // Find first item involved
        while (chunk != null && chunk.rightOffset < offset) {
            chunk = chunk.next
        }

        return chunk
    }

    /**
     * Map this chunk and every following
     */
    fun forEachFollowing(operation: (CachedChunk) -> Unit) {
        var chunk: CachedChunk? = this

        // Apply to every chunk
        while (chunk != null) {
            operation(chunk)
            chunk = chunk.next
        }
    }
}