package io.github.banilaste.fileshare.client

import io.github.banilaste.duck.generated.FileListener
import io.github.banilaste.duck.util.LOGGER

class FileListenerImpl internal constructor() : FileListener() {

    override fun changed(path: String, content: ByteArray) {
        LOGGER.info("$path content changed to : ${content.toString(Charsets.UTF_8)}")

    }

    override fun deleted(path: String) {
        LOGGER.info("$path was deleted")
    }

    override fun expired(path: String) {
        LOGGER.info("$path is no longer being watched")
    }

    companion object {
        private lateinit var instance: FileListenerImpl

        fun get(): FileListenerImpl {
            if (!this::instance.isInitialized) {
                instance = FileListenerImpl()
            }

            return instance
        }
    }
}