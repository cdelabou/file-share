package io.github.banilaste.fileshare.server

import io.github.banilaste.duck.DuckProperties
import io.github.banilaste.duck.generated.DuckProject
import io.github.banilaste.duck.util.LOGGER
import io.github.banilaste.duck.util.LogLevel
import io.github.banilaste.duck.util.parseArgs
import io.github.banilaste.fileshare.common.FaultyDatagramSocket
import io.github.banilaste.fileshare.common.FilesProperties
import java.net.InetAddress
import java.nio.file.Files
import java.nio.file.Paths

fun main(args: Array<String>) {
    val parsedArgs = parseArgs(args)

    // Change log level
    LOGGER.level = LogLevel.valueOf(parsedArgs.getOrDefault("logLevel", "WARNING"))
    LOGGER.colorsEnabled = !parsedArgs.containsKey("noColors")

    // Init UDP server
    DuckProperties.socketFactory = { FaultyDatagramSocket(it, parsedArgs["failRate"]?.toDouble() ?: 0.0) }
    DuckProperties.localAddress = InetAddress.getByName(parsedArgs.getOrDefault("localIp", InetAddress.getLocalHost().hostAddress))
    DuckProject.init(parsedArgs["port"]?.toInt() ?: 12345)

    // Set cache folder
    FilesProperties.folder = parsedArgs.getOrDefault("storage", "./cache")
    Files.createDirectories(Paths.get(FilesProperties.folder))

    val host = FileManagerImpl()

    LOGGER.info("server started, file host at ${ host.identifier }")
}