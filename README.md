
# Remote file access system

This project is remote file access system project, making use of the [duck networking library](https://gitlab.com/banilaste/duck).
It allows to manage files on a server (by reading, writing...) remotely using the client.

## Installation
### Requirements
- Git for cloning the project *(or download this project zip)*
- Maven for installation and building the JAR file
- Java for running the project

During develeppement, Java 8 and Maven 3.6.3 were used.

### Building the project
To build the project, you can use the following commands
```bash
git clone https://gitlab.com/banilaste/file-share.git
cd file-share
mvn install
```
This will clone the project into the current directory, download all the dependencies and finally build the Jar files for both the client and the server.
The executables files are now located into the `target/` folder of the project.

## Running
From the command line, you can now start both the client and the server using java.
### Command line arguments
These command line arguments are used for both the client and the server.

| Program | Argument name | Values | Default | Description | Example |
| --- |---|---|---|---|---|
| All | logLevel | VERBOSE, DEBUG, WARNING, INFO, ERROR or RESULT | WARN | Sets the logging level to the given threshold. All the logs with levels less important than the threshold are not displayed. | \-\-logLevel=VERBOSE |
|All|failRate|From 0 to 1|0|Sets the failure rate for the sent datagrams.|-\-failRate=0.33|
|All|noColors| | |Disable ANSI colors on the logs.|-\-noColors|
|All|localIp|Ip address|Local network ip|Sets the public ip to use for hosted remote objects. **This argument has to be specified if the server and the client are not both on the same network, otherwise the _watch_ command may not work.**|-\-localIp=192.168.1.38|
|Server|storage|Folder path|Current directory|Sets the folder used for as remote file system. If the folder does not exists it is created.|-\-storage=/|
|Server|port|From 0 to 65535|12345|Sets the port used by the server.|-\-port=54321|
|Client|localPort|From 0 to 65535|11111|Sets the port used by the client.|-\-localPort=33333|
|Client|remoteIp|Ip address| |Sets the ip of the server.|-\-remoteIp=192.168.1.40|
|Client|remotePort|From 0 to 65535||Sets the server port used by the server.|-\-remotePort=54321|
|Client|cachingTimeout|Any positive integer|10000|Sets the timeout for expiration of the validity of the file cache, in milliseconds.|-\-cachingTimeout=20000|


### Server

You can run the server without any argument or specify above arguments to customize some properties.
```bash
# Server on port 12345 with current directory as root folder
java -jar ./file_share_server-jar-with-dependencies.jar

# Server on port 54321 with "drive" directory as root folder, a verbose logging and a failure rate of 50%
java -jar ./file_share_server-jar-with-dependencies.jar --storage=drive --port=54321 --logLevel=VERBOSE --failRate=0.5
```

### Client

The same goes for the client, except the server port and ip address will be asked if you do not provide them.
```bash
# Default settings, you will have to input the server ip and port
java -jar ./file_share_client-jar-with-dependencies.jar

# Server at 192.168.1.40:54321, verbose logging and caching timeout of 15s
java -jar ./file_share_client-jar-with-dependencies.jar --remoteIp=192.168.1.40 --remotePort=54321 --logLevel=VERBOSE --cachingTimeout=15000
```

## Command list

Here is the list of commands you can use in the command line interface, writing something that is not a command (like `help` or `qsjkdhqksdh`) will display this list. Arguments with default values (_a = 2_) are optional.

| Name | Arguments | Description|
|---|---|---|
|**read**| file path, offset = 0, _size = remaining length_ |read a file from given offset if provided|
|**write**| file path, _offset = 0_ | write content in a file from a given offset, create file is it does exists|
|**insert**| file path, _offset = 0_ | insert content in a file from a given offset|
|**ls** | _file path = /_ | list content of a directory|
|**mkdir**| file path | create recursively a directory|
|**watch**| file path, duration | watch file content for a given ms duration|
|**sizeof**| file path | returns the size of a file in bytes|
|**crypt**| file path, password | crypt file using a vigenere like algorithm (really secure !)|
|**decrypt**| file path, password | decrypt file using a vigenere like algorithm|
|**delete**| file path | delete file|
